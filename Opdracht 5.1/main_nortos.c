/*
 * Copyright (C) 2018, Hogeschool Rotterdam, Harry Broeders
 * All rights reserved.
 */

#include <stdint.h>
#include <stddef.h>
#include <stdio.h>
#include <NoRTOS.h>

#include <ti/devices/cc32xx/inc/hw_memmap.h>
#include <ti/devices/cc32xx/inc/hw_types.h>
#include <ti/devices/cc32xx/driverlib/prcm.h>
#include <ti/devices/cc32xx/driverlib/i2s.h>
#include <ti/drivers/I2C.h>

#include "Board.h"
#include "config.h"

// You can select the sample rate here:
#define SAMPLINGFREQUENCY 8000
#if SAMPLINGFREQUENCY < 8000 || SAMPLINGFREQUENCY > 48000 || SAMPLINGFREQUENCY % 4000 != 0
#error Sampling Frequency must be between 8 kHz and 48 kHz (included) and must be a multiple of 4 kHz.
#endif

#define N 20

#define BL 21

int16_t bufferlinks[N];
int16_t bufferrechts[N];

const int16_t B[BL] =
{
 1043 , 819 , 0 , -1054 , -1738 , -1475 , 0 , 2458 ,
 5215 , 7375 , 8192 , 7375 , 5215 , 2458 , 0 , -1475 ,
 -1738 , -1054 , 0 , 819 , 1043
};

void main(void)
{
    // Init CC3220S LAUNCHXL board.
    Board_initGeneral();
    // Prepare to use TI drivers without operating system.
    NoRTOS_start();

    int i=0;
    for(i=0;i<N;i++)
    {
        bufferlinks[i]=0;
        bufferrechts[i]=0;
    }

    int32_t output_links = 0;
    int32_t output_rechts = 0;
    int k = 0;

    printf("line-in_2_line_out: STEREO LINE IN ==> HP LINE OUT.\n");
    printf("Sampling frequency = %d Hz.\n", SAMPLINGFREQUENCY);

    // Configure an I2C connection which is used to configure the audio codec.
    I2C_Handle i2cHandle = ConfigureI2C(Board_I2C0, I2C_400kHz);
    // Configure the audio codec.
    ConfigureAudioCodec(i2cHandle, SAMPLINGFREQUENCY);

    // Configure an I2S connection which is use to send/receive samples to/from the codec.
    ConfigureI2S(PRCM_I2S, I2S_BASE, SAMPLINGFREQUENCY);

    unsigned long dataLeft, dataRight;

    while (1)
    {
        // The 16-bit samples are stored in 32-bit variables because the API also supports 24-bit samples.
        I2SDataGet(I2S_BASE, I2S_DATA_LINE_1, &dataLeft);
        bufferlinks[0] = dataLeft;
        output_links = 0;
        for(k=0;k<=N;k++)
        {
            output_links = output_links + (B[k]*bufferlinks[k]);
        }
        for(i=N;i>=1;i--)
        {
            bufferlinks[i]=bufferlinks[i-1];
        }
        I2SDataPut(I2S_BASE, I2S_DATA_LINE_0, (unsigned long)(output_links>>15));


        I2SDataGet(I2S_BASE, I2S_DATA_LINE_1, &dataRight);
        bufferrechts[0] = dataRight;
        output_rechts = 0;
        for(k=0;k<=N;k++)
        {
            output_rechts = output_rechts + (B[k]*bufferrechts[k]);
        }
        for(i=N;i>=1;i--)
        {
            bufferrechts[i]=bufferrechts[i-1];
        }
        I2SDataPut(I2S_BASE, I2S_DATA_LINE_0, (unsigned long)(output_rechts>>15));
    }
}
